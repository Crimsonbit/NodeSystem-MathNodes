package at.crimsonbit.nodesystem.node.math;

import java.math.BigDecimal;

import at.crimsonbit.nodesystem.nodebackend.annotations.NodeField;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeInput;
import at.crimsonbit.nodesystem.nodebackend.annotations.NodeOutput;
import at.crimsonbit.nodesystem.nodebackend.node.AbstractNode;

public class RemainderNode extends AbstractNode {

	@NodeInput
	@NodeField
	BigDecimal dividend = BigDecimal.ZERO;

	@NodeInput
	@NodeField
	BigDecimal divisor = BigDecimal.ONE;

	@NodeOutput("compute")
	BigDecimal output;

	public RemainderNode() {
	}

	@Override
	public void compute() {
		output = dividend.remainder(divisor);

	}

}
